var _ = require('lodash');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

module.exports = function(app, User) {
	app.use(passport.initialize());
	app.use(passport.session());

	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	passport.use(new LocalStrategy({ usernameField: 'email' }, function(email, password, done) {
		User.findOne({email: email}, function(err, user) {
			if (err) return done(err);
			if (!user) return done(null, false);
			user.comparePassword(password, function(err, isMatch) {
				if (err) return done(err);
				if (isMatch) return done(null, user);
				return done(null, false);
			});
		});
	}));

	app.use(function(req, res, next) {
		if (req.user) {
			res.cookie('user', JSON.stringify(req.user), {expires: new Date(Date.now() + 365 * 24 * 60 * 60 * 1000)});
		}
		next();
	});

	app.post('/api/login', passport.authenticate('local'), function(req, res) {
		res.cookie('user', JSON.stringify(req.user), {expires: new Date(Date.now() + 365 * 24 * 60 * 60 * 1000)});

		res.send(req.user);
	});

	app.post('/api/signup', function(req, res, next) {
        var data = _.pick(req.body, 'email', 'password');

        User.findOne({email: data.email}, function(err, user) {
            if (err) return done(err);

            if (user) {
                res.send(500, {code: 'Already registered'});
                return;
            }

            user = new User({
                email: data.email,
                password: data.password
            });
            user.save(function(err) {
                if (err) return next(err);
                res.send(200);
            });
        });
	});

	app.get('/api/logout', function(req, res, next) {
		req.logout();
		res.send(200);
	});
};
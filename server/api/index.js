
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

module.exports = function(app) {
	/**
	 * Mongoose
	 */
	var userSchema = new mongoose.Schema({
		email: {type: String, unique: true},
		password: String,
        demo: {type: Boolean, deafult: false},
        created: {type: Date, default: Date.now }
	});

	userSchema.pre('save', function(next) {
		var user = this;
		if (!user.isModified('password')) return next();
		bcrypt.genSalt(10, function(err, salt) {
			if (err) return next(err);
			bcrypt.hash(user.password, salt, function(err, hash) {
				if (err) return next(err);
				user.password = hash;
				next();
			});
		});
	});

	userSchema.methods.comparePassword = function(candidatePassword, cb) {
		bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
			if (err) return cb(err);
			cb(null, isMatch);
		});
	};

	var categorySchema = new mongoose.Schema({
        type: String,
		name: String,
		user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
		order: Number,
		parent: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'}
	});
	categorySchema.set('collection', 'categories');

	var transactionSchema = new mongoose.Schema({
        type: String,
		date: Date,
		category: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
		sum: Number,
		comment: String,
		user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
	});

	var User = mongoose.model('User', userSchema);
	var Category = mongoose.model('Category', categorySchema);
	var Transaction = mongoose.model('Transaction', transactionSchema);

	mongoose.connect('localhost/money');

	app.checkAuthentication = function(req, res, next) {
		if (req.isAuthenticated()) next();
		else res.send(401, 'Unauthorized');
	};

	require('./auth')(app, User);
	require('./transactions')(app, Transaction);
	require('./categories')(app, Category, Transaction);

	require('./admin')(app, User, Transaction, Category);
};
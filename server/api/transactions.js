var _ = require('lodash'),
	moment = require('moment');

module.exports = function(app, Transaction) {
	app.get('/api/transactions/:dateFrom/:dateTo', app.checkAuthentication, function (req, res, next) {
		var query = Transaction.find();
		var dateFrom = moment(req.params.dateFrom).hours(0).minutes(0).seconds(0);
		var dateTo = moment(req.params.dateTo).hours(0).minutes(0).seconds(0).add(1, 'day');
		query.where({user: req.user.id, date: {'$gte': dateFrom._d, '$lt': dateTo._d}});

		query.exec(function (err, transactions) {
			if (err) return next(err);
			res.send(transactions);
		});
	});

	app.post('/api/transactions', app.checkAuthentication, function (req, res, next) {
        var data = _.pick(req.body, 'date', 'category', 'sum', 'comment', 'type');
		var transaction = new Transaction(data);
		transaction.user = req.user.id;
		transaction.save(function (err) {
			if (err) return next(err);
			res.send(transaction);
		});
	});

	app.put('/api/transactions', app.checkAuthentication, function (req, res, next) {
		Transaction.findById(req.body._id, function (err, transaction) {
			if (!transaction) {
				res.send(500, 'Transaction not found');
				return;
			}
			if (req.user.id != transaction.user) {
				res.send(401, 'Not permitted');
				return;
			}
			var data = _.pick(req.body, 'date', 'category', 'sum', 'comment', 'type');
			_.extend(transaction, data);
			transaction.save(function (err) {
				if (err) return next(err);
				res.send(200);
			});
		});
	});

	app.delete('/api/transactions/:id', app.checkAuthentication, function (req, res, next) {
		Transaction.findById(req.params.id, function (err, transaction) {
			if (!transaction) {
				res.send(500, 'Transaction not found');
				return;
			}
			if (req.user.id != transaction.user) {
				res.send(401, 'Not permitted');
				return;
			}

			transaction.remove(function (err) {
				if (err) return next(err);
				res.send(200);
			});
		});
	});
};
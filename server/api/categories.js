var _ = require('lodash');

module.exports = function(app, Category, Transaction) {
	app.get('/api/categories', app.checkAuthentication, function(req, res, next) {
		var query = Category.find();
		query.where({user: req.user.id});

		query.exec(function(err, categories) {
			if (err) return next(err);
			res.send(categories);
		});
	});

	app.post('/api/categories', app.checkAuthentication, function (req, res, next) {
        var data = _.pick(req.body, 'name', 'parent', 'type');
		var category = new Category(data);
		category.user = req.user.id;
		category.save(function (err) {
			if (err) return next(err);
			res.send(category);
		});
	});

	app.put('/api/categories', app.checkAuthentication, function (req, res, next) {
		Category.findById(req.body._id, function (err, category) {
			if (!category) {
				res.send(500, 'Category not found');
				return;
			}
			if (req.user.id != category.user) {
				res.send(401, 'Not permitted');
				return;
			}
			var data = _.pick(req.body, 'name', 'parent', 'type');
			_.extend(category, data);
			category.save(function (err) {
				if (err) return next(err);
				res.send(200);
			});
		});
	});

	app.delete('/api/categories/:id', app.checkAuthentication, function (req, res, next) {
		Category.findById(req.params.id, function (err, category) {
			if (!category) {
				res.send(500, 'Category not found');
				return;
			}
			if (req.user.id != category.user) {
				res.send(401, 'Not permitted');
				return;
			}

            var query = Transaction.find();
            query.where({category: category._id});

            query.exec(function (err, transactions) {
                if (err) return next(err);

                if (transactions.length) {
                    res.send(200, {error: 'HasTransactions', number: transactions.length});
                    return;
                }

                category.remove(function (err) {
                    if (err) return next(err);
                    res.send(200);
                });
            });
		});
	});
};
var _ = require('lodash'),
    moment = require('moment');

module.exports = function(app, User, Expense, ExpenseCategory) {
    app.get('/api/admin/users/count', function (req, res, next) {
        User.count({}, function(err, count) {
            if (err) return next(err);
            res.send({count: count});
        });
    });

    app.get('/api/admin/expenses/count', function (req, res, next) {
        Expense.count({}, function(err, count) {
            if (err) return next(err);
            res.send({count: count});
        });
    });

    app.get('/api/admin/expenses/categories/count', function (req, res, next) {
        ExpenseCategory.count({}, function(err, count) {
            if (err) return next(err);
            res.send({count: count});
        });
    });
};
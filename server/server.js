var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var app = express();

/**
 * Express middleware
 */
app.set('port', process.env.PORT || 3000);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client/app')));

app.use(session({
    name: 'money',
    secret: 'get much money',
    cookie: {path: '/', httpOnly: true, maxAge: 365 * 24 * 60 * 60 * 1000}
}));

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.send(500, { message: err.message });
});

require('./api')(app);

/**
 * Run application
 */
app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});
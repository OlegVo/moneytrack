
App.controller('AdminStatsCtrl', ['$scope', 'AdminUsers', 'AdminExpenses', 'AdminExpenseCategories', function($scope, AdminUsers, AdminExpenses, AdminExpenseCategories) {
    AdminUsers.count()
        .success(function(data) {
            $scope.users = data.count;
        });

    AdminExpenses.count()
        .success(function(data) {
            $scope.expenses = data.count;
        });

    AdminExpenseCategories.count()
        .success(function(data) {
            $scope.categories = data.count;
        });
}]);
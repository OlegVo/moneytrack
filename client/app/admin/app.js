'use strict';

App.config(['$routeProvider', function($routeProvider) {
	var accessLevels = {
		guest: 'guest',
		user: 'user',
		all: 'all'
	};

	$routeProvider
        .when('/admin_main', {
            templateUrl: 'admin/views/stats.html',
            controller: 'AdminStatsCtrl',
            access: accessLevels.user
        })
        .when('/admin_users', {
            templateUrl: 'admin/views/users.html',
            controller: 'AdminUsersCtrl',
            access: accessLevels.user
        });

    var menu = [
        {route: '/admin_main', label: 'Статистика'},
        {route: '/admin_users', label: 'Пользователи'}
    ];
    _.each(menu, function(item) {
        $('nav ul').eq(0).append('<li ng-if="currentUser" data-match-route="' + item.route + '"><a href="' + item.route + '">' + item.label + '</a></li>');
    });
}]);

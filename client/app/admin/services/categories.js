
App.factory('AdminExpenseCategories', ['$http',
    function($http) {
        return {
			count: function() {
				return $http.get('api/admin/expenses/categories/count');
			}
		};
    }
]);
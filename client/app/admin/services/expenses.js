
App.factory('AdminExpenses', ['$http',
    function($http) {
        return {
			count: function() {
				return $http.get('api/admin/expenses/count');
			}
		};
    }
]);
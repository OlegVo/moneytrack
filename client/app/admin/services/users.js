
App.factory('AdminUsers', ['$http',
    function($http) {
        return {
			count: function() {
				return $http.get('api/admin/users/count');
			}
		};
    }
]);
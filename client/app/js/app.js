'use strict';

var App = angular.module('moneyTracker', ['ngRoute', 'ngResource', 'ngCookies', 'ngMessages', 'mgcrea.ngStrap']);

App.config(['$locationProvider', '$routeProvider', '$httpProvider', function($locationProvider, $routeProvider, $httpProvider) {
	$locationProvider.html5Mode(true);

	var accessLevels = {
		guest: 'guest',
		user: 'user',
		all: 'all'
	};

	$routeProvider
        .when('/start', {
            templateUrl: 'views/start.html',
            controller: 'StartCtrl',
            access: accessLevels.all
        })
        .when('/demo', {
            templateUrl: 'views/demo.html',
            controller: 'DemoCtrl',
            access: accessLevels.guest
        })
        .when('/about', {
            templateUrl: 'views/about.html',
            controller: 'AboutCtrl',
            access: accessLevels.all
        })
		.when('/expenses', {
			templateUrl: 'views/transactions.html',
			controller: 'TransactionsCtrl',
			access: accessLevels.user
		})
		.when('/categories', {
			templateUrl: 'views/categories.html',
			controller: 'CategoriesCtrl',
			access: accessLevels.user
		})
		.when('/reports', {
			templateUrl: 'views/reports.html',
			controller: 'ReportsCtrl',
			access: accessLevels.user
		})
		.when('/feedback', {
			templateUrl: 'views/feedback.html',
			controller: 'FeedbackCtrl',
			access: accessLevels.all
		})
		.when('/login', {
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl',
			access: accessLevels.guest
		})
		.when('/signup', {
			templateUrl: 'views/signup.html',
			controller: 'SignupCtrl',
			access: accessLevels.guest
		})
		.otherwise({
			redirectTo: '/expenses'
		});

	// разлогинивание при получении от сервера 401
	$httpProvider.interceptors.push('httpInterceptor');

	moment.lang('ru');

    _.mixin(_.str.exports());
}]);

App.run(['$rootScope', '$location', 'User', 'Analytics', function($rootScope, $location, User, Analytics) {
    Analytics.initGoogle();
    Analytics.initYandex();

	$rootScope.$on('$routeChangeStart', function(event, next) {
		if (!User.authorize(next.access)) {
			if (User.isLoggedIn()) {
				$location.path('/');
			} else {
				$location.path('/start');
			}
		}
	});

	$rootScope.register = function() {
		User.logout('/signup');
	};
}]);

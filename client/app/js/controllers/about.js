'use strict';

App.controller('AboutCtrl', ['$scope', 'Analytics', function($scope, Analytics) {
    Analytics.event('Page', 'Open', 'О программе');

    $scope.clickRegister = function() {
        Analytics.event('About page', 'Click register');
    };
}]);

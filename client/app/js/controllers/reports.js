'use strict';

App.controller('ReportsCtrl', ['$scope', 'Categories', 'Transactions', 'Reports', 'Analytics', '$cookieStore',
    function($scope, Categories, Transactions, Reports, Analytics, $cookieStore) {

    Analytics.event('Page', 'Open', 'Отчёты');

    $scope.transactionType = $cookieStore.get('reportTransactionType') || 'expense';
	var transactions, categories;

	function reloadReport() {
        $scope.report = undefined;

        Categories.categories()
			.success(function(data) {
				categories = Categories.buildTree(data);

				Transactions.transactions($scope.dateFrom, $scope.dateTo)
					.success(function(data) {
						transactions = data;

                        _.each(transactions, function(t) {
                            t.category = _.find(categories, {_id: t.category});
                            t.date = moment(t.date);
                        });

                        makeReport();

						drawGraph();
					});
			});
	}

    var total;
    function makeReport() {
        var result = Reports.makeReport(categories, transactions, $scope.transactionType);

        $scope.report = result.report;
        total = result.total;
        $scope.total = result.totalString;
    }

    function drawGraph(quick) {
        Reports.drawGraph({
            quick: quick,
            updateScope: function() {
                setTimeout(function() {
                    $scope.$apply();
                });
            }
        }, $scope);
    }

	$scope.periodChanged = function() {
		if ($scope.period == 'month') {
			$scope.dateFrom = moment().date(1);
			$scope.dateTo = moment().date(1).add(1, 'month').subtract(1, 'day');
		}
        else if ($scope.period == 'recentMonth') {
            $scope.dateFrom = moment().date(1).subtract(1, 'month');
            $scope.dateTo = moment().date(1).subtract(1, 'day');
        }
		else if ($scope.period == 'any') {
			setTimeout(function() {
				$('#inputDateFrom').off('changeDate').on('changeDate', function (e) {
					$scope.dateFrom = moment(e.date);
					$cookieStore.put('reportDateFrom', $scope.dateFrom.format('YYYY-MM-DD'));

					reloadReport();
				});

				$('#inputDateTo').off('changeDate').on('changeDate', function (e) {
					$scope.dateTo = moment(e.date);
					$cookieStore.put('reportDateTo', $scope.dateTo.format('YYYY-MM-DD'));

					reloadReport();
				});
			}, 10);
		}

        $cookieStore.put('reportPeriod', $scope.period);
        $cookieStore.put('reportDateFrom', $scope.dateFrom.format('YYYY-MM-DD'));
        $cookieStore.put('reportDateTo', $scope.dateTo.format('YYYY-MM-DD'));

		reloadReport();
	};

	$scope.periods = [
		{name: 'month', label: 'Этот месяц'},
		{name: 'recentMonth', label: 'Прошлый месяц'},
		{name: 'any', label: 'Другой период'}
	];
	$scope.period = $cookieStore.get('reportPeriod') || $scope.periods[0].name;

	$scope.dateFrom = $cookieStore.get('reportDateFrom') ? moment($cookieStore.get('reportDateFrom')) : moment().date(1);
	$scope.dateTo = $cookieStore.get('reportDateTo') ? moment($cookieStore.get('reportDateTo')) : moment().date(1).add(1, 'month').subtract(1, 'day');

    $scope.periodChanged();

    $scope.foldCategory = function(line) {
        if (!line.category.canBeFolded) return;

        var foldCategory = function(line) {
            var fold = !line.category.folded;

            if (fold) foldChildren(line.category);

            line.category.folded = fold;

            line.sum = fold ? line.category.total : line.category.sum;
            line.sumString = _.numberFormat(line.sum, 0, ',', ' ');
            line.percent = line.sum * 100 / total;
        };

        var foldChildren = function(c) {
            if (!c.children || !c.children.length) return;

            _.each(c.children, function(c) {
                c.folded = true;
                foldChildren(c);
            });
        };

        foldCategory(line);

        drawGraph(true);
    };

    $scope.setTransactionType = function(type) {
        $cookieStore.put('reportTransactionType', type);

        if (type != $scope.transactionType) {
            $scope.transactionType = type;

            makeReport();
            drawGraph();
        }
    };
}]);
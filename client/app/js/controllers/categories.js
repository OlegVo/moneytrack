'use strict';

App.controller('CategoriesCtrl', ['$scope', 'Categories', 'Analytics', '$cookieStore', function($scope, Categories, Analytics, $cookieStore) {
    Analytics.event('Page', 'Open', 'Категории');

	// is c1 child of c2
	function isChild(c1, c2) {
		if (!c1.parent) return false;

		if (c1.parent == c2) return true;

		return isChild(c1.parent, c2);
	}

    Categories.categories()
		.success(function(categories) {
            $scope.categories = Categories.buildTree(categories);
		});

	$scope.editCategory = function(category) {
        $scope.cancelEditing();

		$scope.edited = category || _.last($scope.categories);
		$scope.edited.active = true;
        if (!$scope.edited.new) {
            $scope.edited.previousData = _.pick($scope.edited, 'name', 'parent');
        }

		$scope.categories = _.reject($scope.categories, function(c) {
			return !c.name && !c.active;
		});

		$scope.categoriesToSelect = _.filter($scope.categories, function(c) {
			return c !== $scope.edited && c.type == $scope.categoryType && !isChild(c, $scope.edited);
		});

		if ($scope.edited.new) {
			setTimeout(function() {
                $('#edit_category').find('[name=name]').focus();
			});
		}

        // это для селектора категорий
        $scope.categoryName = $scope.edited.parent && $scope.edited.parent.name || '';
	};

    $scope.cancelEditing = function() {
        if (!$scope.edited) return;

        if ($scope.edited.new) {
            $scope.removeEditedFromList();
        } else {
            // если до этого действия редактировали другую транзакцию, восстанавливаем прежние значения
            if ($scope.edited.previousData) {
                _.each($scope.edited.previousData, function(value, property) {
                    $scope.edited[property] = value;
                });
                delete $scope.edited.previousData;

                // обновляем дерево категорий
                $scope.categories = Categories.buildTree($scope.categories);
            }

            $scope.edited.active = false;
            delete $scope.edited;
        }
    };

    $scope.selectCategory = function(category) {
		$scope.edited.parent = category && category.id || null;

        $scope.categories = Categories.buildTree($scope.categories);
	};

	$scope.addCategory = function() {
		$scope.categories.push({new: true, type: $scope.categoryType});
		$scope.editCategory();
	};

	$scope.saveCategory = function() {
        Categories.save($scope.edited);
		$scope.edited.new = false;
		$scope.edited.active = false;
		delete $scope.edited;
	};

	$scope.deleteCategory = function() {
        Categories.remove($scope.edited, function(err) {
            if (!err) {
                $scope.removeEditedFromList()
            }
        });
	};

	$scope.removeEditedFromList = function() {
		$scope.categories = _.without($scope.categories, $scope.edited);
		delete $scope.edited;
	};

    $scope.categoryType = $cookieStore.get('transactionType') || 'expense';
    $scope.setCategoryType = function(type) {
        $cookieStore.put('transactionType', type);

        if (type != $scope.categoryType) {
            $scope.categoryType = type;

            if ($scope.edited && $scope.edited.new) {
                $scope.edited.type = $scope.categoryType;
            }
        }
    };
}]);

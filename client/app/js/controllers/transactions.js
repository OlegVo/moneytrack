'use strict';

App.controller('TransactionsCtrl', ['$scope', 'Transactions', 'Categories', 'Analytics', '$cookieStore', function($scope, Transactions, Categories, Analytics, $cookieStore) {
    Analytics.event('Page', 'Open', 'Расходы');

    var transactionsByMonth = {};

    function loadCategories(callback) {
        if ($scope.categories) {
            callback();
            return;
        }

        Categories.categories()
            .success(function(categories) {
                $scope.categories = Categories.buildTree(categories);

                callback();
            });
    }

	function loadTransactionsForMonth(date, callback) {
        var month = date.format('M-YYYY');
        if (transactionsByMonth[month]) {
            callback();
            return;
        }

        var date1 = moment(date).date(1);
        var date2 = moment(date1).add(1, 'month').subtract(1, 'day');
        Transactions.transactions(date1, date2)
            .success(function(transactions) {
                month = date.format('M-YYYY');
                transactionsByMonth[month] = {};

                var dayOfMonth = moment(date1);
                var monthNum = dayOfMonth.month();
                while (dayOfMonth.month() == monthNum) {
                    transactionsByMonth[month][dayOfMonth.date()] = {
                        date: moment(dayOfMonth),
                        transactions: [],
                        expensesSum: 0,
                        incomesSum: 0
                    };
                    dayOfMonth.add(1, 'day');
                }

                _.each(transactions, function(t) {
                    t.date = moment(t.date);
                    transactionsByMonth[month][t.date.date()].transactions.push(t);
                });

                updateMonthSums(date1);

                callback();
            });
	}

    function updateMonthSums(date) {
        var month = date.format('M-YYYY');

        _.each(transactionsByMonth[month], updateDaySums);
    }

    function updateDaySums(dayData) {
        dayData.expensesSum = 0;
        dayData.incomesSum = 0;
        _.each(dayData.transactions, function(t) {
            var sum = parseFloat(t.sum) || 0;
            if (t.type == 'expense') {
                dayData.expensesSum += sum;
            }
            if (t.type == 'income') {
                dayData.incomesSum += sum;
            }
        });
        dayData.expensesString = _.numberFormat(dayData.expensesSum, 0, ',', ' ');
        dayData.incomesString = _.numberFormat(dayData.incomesSum, 0, ',', ' ');
    }

    function loadTransactionsForWeek(callback) {
        var firstDayOfWeek = moment($scope.date).startOf('week'),
            lastDayOfWeek = moment($scope.date).endOf('week');
        var fromOneMonth = firstDayOfWeek.month() == lastDayOfWeek.month();
        $scope.weekTransactions = [];
        $scope.weekPeriod = firstDayOfWeek.format(fromOneMonth ? 'D' : 'D MMMM') + ' - ' + lastDayOfWeek.format('D MMMM');

        loadTransactionsForMonth(firstDayOfWeek, function() {
            loadTransactionsForMonth(lastDayOfWeek, function() {
                var dayOfWeek = moment(firstDayOfWeek);
                for (var i = 0; i < 7; i++) {
                    var dayTransactions = transactionsByMonth[dayOfWeek.format('M-YYYY')][dayOfWeek.date()];
                    $scope.weekTransactions.push(dayTransactions);
                    dayOfWeek.add(1, 'day');
                }

                callback();
            });
        });
    }

    function loadTransactionsForDate(callback) {
        loadTransactionsForWeek(function() {
            $scope.transactions = transactionsByMonth[$scope.date.format('M-YYYY')][$scope.date.date()].transactions;

            _.each($scope.transactions, function(e) {
                _.isObject(e.category) || (e.category = _.find($scope.categories, {_id: e.category}));
            });

            if ($scope.edited && $scope.edited.new) {
                $scope.transactions.push($scope.edited);
            }

            callback();
        });
    }

    function updateTransactionsListForDate(date) {
        var dayData = transactionsByMonth[date.format('M-YYYY')][date.date()];
        dayData.transactions = _.filter(dayData.transactions, function(t) {
            return t.date.format('DDMMYYYY') === date.format('DDMMYYYY');
        });
    }

    function updateSumsForDate(date) {
        var dayData = transactionsByMonth[date.format('M-YYYY')][date.date()];
        updateDaySums(dayData);
    }

    function addTransaction(transaction) {
        var date = transaction.date;
        var dayData = transactionsByMonth[date.format('M-YYYY')][date.date()];

        if (!_.contains(dayData.transactions, transaction)) {
            dayData.transactions.push(transaction);
        }
    }

    function removeTransaction(transaction) {
        var date = transaction.date;
        var dayData = transactionsByMonth[date.format('M-YYYY')][date.date()];
        _.pull(dayData.transactions, transaction);
        updateSumsForDate(date);
    }

    // обновить список транзакций и суммы расходов и доходов
    // на дату транзакции и на текущую дату
    function updateTransactionInCalendar(transaction) {
        if (transaction.date.format('DDMMYYYY') != $scope.date.format('DDMMYYYY')) {
            addTransaction(transaction);
            updateTransactionsListForDate($scope.date);
            updateSumsForDate($scope.date);
        }
        updateSumsForDate(transaction.date);

        var dayData = transactionsByMonth[$scope.date.format('M-YYYY')][$scope.date.date()];
        $scope.transactions = dayData.transactions;
    }

	$scope.date = $cookieStore.get('date') ? moment($cookieStore.get('date')) : moment();
    dateChanged();

	var $inputDate = $('#inputDate');

    function dateChanged() {
        $cookieStore.put('date', $scope.date.format('YYYY-MM-DD'));

        if ($scope.edited) {
            $scope.cancelEditing();
        }
        loadCategories(function() {
            loadTransactionsForDate(function() {
                makeCalendar();
            });
        });
    }

	setTimeout(function() {
		$inputDate.on('changeDate', function (e) {
			$scope.edited.date = moment(e.date);
		});
	});

	$scope.editTransaction = function(transaction) {
        $scope.cancelEditing();

		$scope.edited = transaction || _.last($scope.transactions);
        $scope.edited.active = true;
        if (!$scope.edited.new) {
            Analytics.event('Transactions', 'Edit transaction', $scope.edited.type);

            $scope.edited.previousData = _.pick($scope.edited, 'date', 'category', 'sum', 'comment');
        }

		if ($scope.edited.new) {
			setTimeout(function() {
                $('#edit_transaction').find('[name=category]').focus();
			});
		}

        $scope.categoryName = $scope.edited.category && $scope.edited.category.name || '';

		$inputDate.datepicker('setDate', $scope.edited.date._d);
		$inputDate.datepicker('update');
	};

    $scope.cancelEditing = function() {
        if (!$scope.edited) return;

        if ($scope.edited.new) {
            Analytics.event('Transactions', 'Cancel addition');

            $scope.removeEditedFromList();
        } else {
            Analytics.event('Transactions', 'Cancel editing');

            // если до этого действия редактировали другую транзакцию, восстанавливаем прежние значения
            if ($scope.edited.previousData) {
                _.each($scope.edited.previousData, function(value, property) {
                    $scope.edited[property] = value;
                });
                delete $scope.edited.previousData;
            }

            $scope.edited.active = false;
            delete $scope.edited;
        }
    };

	$scope.selectCategory = function(category) {
		$scope.edited.category = category || null;
	};

    $scope.addTransaction = function() {
        Analytics.event('Transactions', 'Add transaction', $scope.transactionType);

        if ($scope.edited && $scope.edited.new) return;

        addTransaction({
            new: true,
            type: $scope.transactionType,
            date: moment($scope.date)
        });
        $scope.editTransaction();
    };

    $scope.saveTransaction = function() {
        var transaction = $scope.edited;
        if (!transaction.category && !transaction.sum) {
            $scope.deleteTransaction();
            return;
        }

        transaction.sum = parseInt(transaction.sum);
        if (transaction.sum !== transaction.sum) transaction.sum = 0;

		Transactions.save(transaction);

        var addOneMore = transaction.new;
        transaction.new = false;
        transaction.active = false;

        updateTransactionInCalendar(transaction);

		delete $scope.edited;

        if (addOneMore) $scope.addTransaction();
    };

    $scope.deleteTransaction = function() {
        if (!$scope.edited.new) {
            Analytics.event('Transactions', 'Delete transaction', $scope.edited.type);
            Transactions.remove($scope.edited)
                .success(function () {
                    $scope.removeEditedFromList();
                });
        } else {
            $scope.removeEditedFromList();
        }
    };

	$scope.removeEditedFromList = function() {
        removeTransaction($scope.edited);
		delete $scope.edited;
	};

    $scope.createCategory = function(name, callback) {
        var category = {
            name: name,
            type: $scope.edited.type,
            new: true
        };

        Categories.save(category, function(category) {
            $scope.categories.push(category);
            $scope.categories = Categories.buildTree($scope.categories);

            callback(category);
        });
    };

    $scope.changeDate = function(date) {
        $scope.date = moment(date);
        dateChanged();
        Analytics.event('Transactions', 'Pick date');
    };

    $scope.previousMonth = function() {
        $scope.date.subtract(1, 'month');
        dateChanged();
        Analytics.event('Transactions', 'Previous month');
    };
    $scope.nextMonth = function() {
        $scope.date.add(1, 'month');
        dateChanged();
        Analytics.event('Transactions', 'Next month');
    };

    $scope.transactionType = $cookieStore.get('transactionType') || 'expense';
    $scope.setTransactionType = function(type) {
        if (type != $scope.transactionType) {
            $scope.transactionType = type;

            if ($scope.edited && $scope.edited.new) {
                $scope.edited.type = $scope.transactionType;
            }
        }
    };

    var setCategoriesOfType = function() {
        $scope.categoriesOfType = _.filter($scope.categories, function(c) {
            return c.type === $scope.transactionType;
        });
    };
    $scope.$watch('transactionType', function(value) {
        $cookieStore.put('transactionType', value);
        setCategoriesOfType();
    });
    $scope.$watch('categories', setCategoriesOfType);
    $scope.$watch('date', function(value) {
        $cookieStore.put('date', value.format('YYYY-MM-DD'));
    });

    var makeCalendar = function() {
        if ($scope.calendar && $scope.calendar.date.format('DDMMYY') == $scope.date.format('DDMMYY')) return;

        $scope.calendar = {
            date: moment($scope.date),
            month: $scope.date.format('M-YYYY'),
            monthString: _.capitalize($scope.date.format('MMMM, YYYY')),
            weeks: []
        };
        var monthTransactions = transactionsByMonth[$scope.calendar.month];

        var date = moment($scope.date).startOf('month').startOf('week');
        var currentMonth = $scope.date.month();
        var nextMonth = moment($scope.date).add(1, 'month').month();
        while (true) {
            if (date.day() == 1) {
                $scope.calendar.weeks.push({days: []});
            }

            var calendarDay = {date: moment(date)};

            if (date.month() == currentMonth) {
                calendarDay.currentMonth = true;
                calendarDay.transactionsData = monthTransactions[date.date()];
            }

            _.last($scope.calendar.weeks).days.push(calendarDay);

            date.add(1, 'day');

            if (date.month() == nextMonth && date.day() == 1) {
                break;
            }
        }
    };

    $scope.today = moment();
    $scope.showCalendar = function() {
        $scope.cancelEditing();
    };
    $scope.calendarPickDate = function(date) {
        $scope.changeDate(date);
    };
}]);


App.controller('LoginCtrl', ['$scope', 'User', 'Analytics', function($scope, User, Analytics) {
    Analytics.event('Page', 'Open', 'Авторизация');

    $scope.login = function() {
        User.login({
            email: $scope.email,
            password: $scope.password
        });
    };

    setTimeout(function() {
        // включаем кнопку, если поля заполнились автокомплитом
        $scope.$apply(function() {
            if ($('#email').val() != $scope.email) {
                $scope.email = $('#email').val();
            }
            if ($('#password').val() != $scope.email) {
                $scope.password = $('#password').val();
            }
        });
    }, 500);
}]);
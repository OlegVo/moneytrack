App.controller('NavbarCtrl', ['$scope', 'User', function($scope, User) {
    $scope.logout = function() {
        User.logout();
    };
}]);
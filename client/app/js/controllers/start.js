'use strict';

App.controller('StartCtrl', ['$scope', 'User', 'Analytics', function($scope, User, Analytics) {
    Analytics.event('Page', 'Open', 'Стартовая страница');

    $scope.clickRegister = function() {
        Analytics.event('Start page', 'Click register');
    };
}]);

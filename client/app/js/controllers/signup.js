App.controller('SignupCtrl', ['$scope', 'User', 'Analytics', function($scope, User, Analytics) {
    Analytics.event('Page', 'Open', 'Регистрация');

    $scope.email = '';
    $scope.password = '';

    $scope.signup = function() {
        User.signup({
            email: $scope.email,
            password: $scope.password
        });
    };
}]);

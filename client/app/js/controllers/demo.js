'use strict';

App.controller('DemoCtrl', ['$scope', '$location', 'User', 'Analytics', function($scope, $location, User, Analytics) {
    Analytics.event('Start page', 'Click demo');
    Analytics.event('Page', 'Open', 'Демо');

    if ($scope.currentUser) {
        Analytics.event('Demo', 'Open demo again');
        $location.path('/');
    } else {
        Analytics.event('Demo', 'Create new demo user');
        User.loginAsDemoUser();
    }
}]);

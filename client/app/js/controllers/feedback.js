
App.controller('FeedbackCtrl', ['$scope', 'Analytics', 'Mailer', function($scope, Analytics, Mailer) {
	Analytics.event('Page', 'Open', 'Задать вопрос');

	$scope.email = $scope.currentUser && !$scope.currentUser.demo ? $scope.currentUser.email : '';

	$scope.sendMessage = function() {
		$scope.disabled = true;
		Mailer.sendFeedback($scope.message, $scope.email)
			.success(function() {
				$scope.disabled = false;
				$scope.sent = true;
			})
			.error(function() {
				$scope.disabled = false;
			});
	};
}]);

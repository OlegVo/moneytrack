
App.factory('httpInterceptor', ['$q', '$location', '$rootScope', function($q, $location, $rootScope) {
	return {
		'response': function(response) {
			return response;
		},

		'responseError': function(response) {
			if (response.status === 401) {
				$rootScope.currentUser = null;
				$location.path('/login');
				return $q.reject(response);
			}
			else {
				return $q.reject(response);
			}
		}
	};
}]);
App.factory('User', ['$http', '$location', '$rootScope', '$cookieStore', '$alert', 'Analytics',
    function($http, $location, $rootScope, $cookieStore, $alert, Analytics) {
        $rootScope.currentUser = $cookieStore.get('user');

		var userRoles = {
			guest: 'guest',
			user: 'user'
		};
		var accessLevels = {
			guest: 'guest',
			user: 'user',
			all: 'all'
		};

        var User = {
            login: function(user) {
                return $http.post('/api/login', user)
                    .success(function(data) {
                        Analytics.event('Login', 'Success', user.email);

                        $rootScope.currentUser = data;
                        $location.path('/');
                    })
                    .error(function() {
                        Analytics.event('Login', 'Error', user.email);

                        $alert({
                            title: '',
                            content: 'Неверный email или пароль.',
                            placement: 'top-right',
                            type: 'danger',
                            duration: 3
                        });
                    });
            },

            signup: function(user) {
                return $http.post('/api/signup', user)
                    .success(function() {
                        Analytics.event('Registration', 'Success', user.email);

                        $alert({
                            title: '',
                            content: 'Ваш новый аккаунт успешно создан.',
                            placement: 'top-right',
                            type: 'success',
                            duration: 3
                        });

                        User.login(user);
                    })
                    .error(function(response) {
                        Analytics.event('Registration', 'Error', user.email + ', ' + response.code);

                        var message = response.code && response.code == 'Already registered' ? 'У вас уже есть аккаунт на MoneyTrack.' : 'При создании аккаунта произошла ошибка.';
                        $alert({
                            title: '',
                            content: message,
                            placement: 'top-right',
                            type: 'danger',
                            duration: 3
                        });
                    });
            },

            logout: function(newLocation) {
                return $http.get('/api/logout').success(function() {
                    $rootScope.currentUser = null;
                    $cookieStore.remove('user');

					$location.path(newLocation || '/');
                });
            },

			// проверяет что текущему пользователю доступен данный уровень доступа
			authorize: function(accessLevel) {
				var role = $rootScope.currentUser ? userRoles.user : userRoles.guest;
				return accessLevel == accessLevels.all || accessLevel == role;
			},

			isLoggedIn: function() {
				return !!$rootScope.currentUser;
			},

            loginAsDemoUser: function() {
                return $http.post('/api/create_demo_user')
                    .success(function(user) {
                        Analytics.event('Demo', 'User Created', user.email);

                        User.login(user);
                    })
                    .error(function(response) {
                        Analytics.event('Demo', 'User Creation Error', response.code);
                    });
            }
        };

        return User;
    }]);
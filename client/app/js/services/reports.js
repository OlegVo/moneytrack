
App.factory('Reports', [
    function() {
        var Reports = {
            makeReport: function(categories, transactions, transactionType) {
                var transactionsOfType = _.filter(transactions, {type: transactionType});

                // проставляем сумму в каждой категории
                _.each(categories, function(c) {
                    c.sum = 0;
                    c.total = 0;
                });
                _.each(transactionsOfType, function(e) {
                    if (e.sum) {
                        e.category.sum += e.sum;
                    }
                    e.category.total = 0;
                });

                // считаем сумму по вложенным категориям
                var addSumToParentCategory = function(c, sum) {
                    c.parent.total += sum;
                    if (c.parent.parent) {
                        addSumToParentCategory(c.parent, sum);
                    }
                };
                var total = 0;
                _.each(categories, function(c) {
                    c.sum || (c.sum = 0);
                    total += c.sum;

                    c.total || (c.total = 0);
                    c.total += c.sum;
                    if (c.sum && c.parent) {
                        addSumToParentCategory(c, c.sum);
                    }
                });

                var report = [];
                _.each(categories, function(c) {
                    if (c.type == transactionType && c.total) {
                        var line = {
                            category: c,
                            label: c.name,
                            percent: c.total * 100 / total,
                            sum: c.total,
                            sumString: _.numberFormat(c.total, 0, ',', ' ')
                        };

                        report.push(line);

                        c.canBeFolded = _.some(c.children, function(c) {
                            return c.total;
                        });
                        c.folded = true;
                    }
                });

                return {
                    report: report,
                    total: total,
                    totalString: _.numberFormat(total, 0, ',', ' ')
                };
            },

            drawGraph: function(params, $scope) {
                var report = $scope.report;
                var updateScope = params.updateScope;
                var animationDelay = params.quick ? 0 : 300,
                    sectorDrawDelay = params.quick ? 0 : 150;
                var colorNum = 0;

                Raphael.fn.pieChart = function(report) {
                    var cx = 400, cy = 250, r = 180;

                    var sectorIndex = 0;
                    function makeSector(cx, cy, r, startAngle, endAngle, params, callback) {
                        setTimeout(function() {
                            var path = function(radius) {
                                var x1 = cx + radius * Math.sin(startAngle * rad),
                                    y1 = cy - radius * Math.cos(startAngle * rad),
                                    x2 = cx + radius * Math.sin(endAngle * rad),
                                    y2 = cy - radius * Math.cos(endAngle * rad);
                                return ['M', cx, cy, 'L', x1, y1, 'A', radius, radius, 0, +(endAngle - startAngle > 180), 1, x2, y2, 'z'];
                            };

                            var sector;
                                if (params.quick) {
                                sector = paper.path(path(r)).attr(params);
                            } else {
                                sector = paper.path(path(1)).attr(params);
                                var anim = Raphael.animation({path: path(r)}, animationDelay, 'easeOut');
                                sector.animate(anim);
                            }

                            setTimeout(function() {
                                callback(sector);
                            }, animationDelay);
                        }, sectorIndex++ * sectorDrawDelay)
                    }

                    var paper = this,
                        rad = Math.PI / 180,
                        chart = this.set();

                    var angle = 0,
                        total = 0;

                    var colors = [
                        [0.59, 0.61, 0.71],
                        [0.07, 0.73, 0.82],
                        [0.3, 0.6, 0.72],
                        [0.87, 0.6, 0.72],
                        [0.68, 0.6, 0.72],
                        [0.5, 0.6, 0.72],
                        [0, 0.6, 0.72]
                    ];
                    var color1, color2;

                    var textsPositions = [];

                    function drawSector(line) {
                        var value = line.sum,
                            angleplus = 360 * value / total,
                            popangle = angle + (angleplus / 2),
                            delta = 55;

                        var percent = line.percent < 10 ? _.numberFormat(line.percent, 1, ',', ' ') : Math.round(line.percent);
                        var label = line.label + ', ' + percent + '%';

                        var lineColors = line.category.colors;
                        if (!lineColors) {
                            color1 = colors[colorNum] || colors[colorNum % colors.length];
                            if (colorNum >= colors.length) color1[0] += 0.14 * Math.floor(colorNum / colors.length);
                            color2 = [color1[0], color1[1] / 1.3, color1[2] + (1 - color1[2]) * 0.8];

                            var color1str = Raphael.hsb(color1[0], color1[1], color1[2]);
                            var color2str = Raphael.hsb(color2[0], color2[1], color2[2]);

                            lineColors = line.category.colors = [color1str, color2str];
                        }

                        var gradientAngle = parseInt(angle + angleplus / 2);
                        makeSector(cx, cy, r, angle, angle + angleplus, {
                            fill: gradientAngle + '-' + lineColors[1] + '-' + lineColors[0],
                            stroke: '#fff',
                            'stroke-width': 1
                        }, function(sector) {
                            var x = cx - (r + delta + 30) * Math.sin(-popangle * rad),
                                y = cy - (r + delta) * Math.cos(-popangle * rad);

                            var skipText;
                            if (textsPositions.length) {
                                var lastTextPos = _.last(textsPositions);
                                var yDelta = Math.abs(y - lastTextPos.y);

                                if (yDelta < 18 || popangle < lastTextPos.angle) {
                                    y = lastTextPos.y + 18 * (popangle < 180 ? 1 : -1);

                                    var cos = (cy - y) / (r + delta);
                                    if (cos > 1 || cos < -1) skipText = true;

                                    if (!skipText) {
                                        popangle = Math.acos(cos) / rad;
                                        if (cx - x > 0) popangle = 360 - popangle;
                                        x = cx - (r + delta + 30) * Math.sin(-popangle * rad);
                                    }
                                }
                            }

                            var text;
                            if (!skipText) {
                                textsPositions.push({
                                    angle: popangle,
                                    x: x,
                                    y: y
                                });

                                text = paper.text(x, y, label)
                                    .attr({fill: lineColors[0], stroke: 'none', opacity: 0, 'font-size': 14});
                                text.stop().animate({opacity: 1}, animationDelay, 'linear');
                            }

                            sector.highlight = function () {
                                sector.stop().animate({transform: 's1.05 1.05 ' + cx + ' ' + cy, opacity: 0.9}, 100, 'linear');

                                line.highlighted = true;

                                updateScope();
                            };
                            sector.unhighlight = function () {
                                sector.stop().animate({transform: '', opacity: 1}, 300, 'linear');

                                line.highlighted = false;

                                updateScope();
                            };
                            sector.mouseover(sector.highlight).mouseout(sector.unhighlight);
                            if (text) text.mouseover(sector.highlight).mouseout(sector.unhighlight);

                            line.graph || (line.graph = {});
                            line.graph.sector = sector;

                            chart.push(sector);
                            if (text) chart.push(text);
                        });

                        angle += angleplus;
                        colorNum++;
                    }

                    _.each(report, function(line) {
                        total += line.sum;
                    });
                    _.each(report, function(line) {
                        drawSector(line);
                    });
                    return chart;
                };

                $scope.graph || ($scope.graph = Raphael('graph', 800, 500));
                $scope.graph.clear();

                var reportToDraw = _.filter(report, function(line) {
                    return line.sum && (line.category.parent && !line.category.parent.folded || !line.category.parent);
                });
                if (reportToDraw.length > 1) {
                    $scope.graph.pieChart(reportToDraw);
                }
            }
        };

        return Reports;
    }
]);
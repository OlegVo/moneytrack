
App.factory('Transactions', ['$http',
    function($http) {
        return {
            transactions: function(dateFrom, dateTo) {
				dateTo || (dateTo = moment(dateFrom));

				var zone = dateFrom.zone();
                dateFrom.hours(0).minutes(0).seconds(0).add(-zone, 'minutes').zone(0);
                dateTo.hours(0).minutes(0).seconds(0).add(-zone, 'minutes').zone(0);

				return $http.get('api/transactions/' + dateFrom.format() + '/' + dateTo.format());
			},

			save: function(transaction) {
				var data = _.pick(transaction, '_id', 'sum', 'comment', 'date', 'type');
				data.category = transaction.category ? transaction.category.id : null;

				var zone = transaction.date.zone();
				data.date = transaction.date.add(-zone, 'minutes').zone(0).format();

				if (transaction.new) {
					return $http.post('api/transactions', data)
						.success(function(data) {
							transaction._id = data._id;
						});
				} else {
					return $http.put('api/transactions', data);
				}
			},

			remove: function(transaction) {
				return $http.delete('api/transactions/' + transaction._id);
			}
		};
    }
]);
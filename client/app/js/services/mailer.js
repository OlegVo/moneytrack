
App.factory('Mailer', ['$http', 'Analytics',
	function($http, Analytics) {
		return {
			sendFeedback: function(message, userEmail) {
				var data = {
					message: message,
					userEmail: userEmail
				};

				return $http.post('api/feedback', data)
					.success(function() {
						Analytics.event('Feedback', 'Success', userEmail);
					})
					.error(function() {
						Analytics.event('Feedback', 'Error', userEmail);
					});
			}
		}
	}
]);
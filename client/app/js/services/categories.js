
App.factory('Categories', ['$http', '$alert',
    function($http, $alert) {
        return {
			categories: function() {
				return $http.get('api/categories');
			},

			save: function(category, callback) {
				var data = _.pick(category, '_id', 'name', 'type');
				data.parent = category.parent ? category.parent._id : null;

				if (category.new) {
					return $http.post('api/categories', data)
						.success(function(data) {
							category._id = data._id;
							category.id = data._id;

                            if (callback) callback(category);
						});
				} else {
					return $http.put('api/categories', data)
                        .success(function() {
                            if (callback) callback();
                        });
				}
			},

			remove: function(category, callback) {
                return $http.delete('api/categories/' + category._id)
                    .success(function(response) {
                        if (response.error && response.error === 'HasTransactions') {
                            if (callback) callback(response.error);

                            $alert({
                                title: '',
                                content: 'Нельзя удалить категорию. В категории есть транзакции.',
                                placement: 'top-right',
                                type: 'danger',
                                duration: 3
                            });
                            return;
                        }

                        if (callback) callback();
                    });
			},

            buildTree: function(categories) {
                _.each(categories, function(c) {
                    if (c.parent && typeof(c.parent) !== 'object') {
                        c.parent = _.find(categories, {_id: c.parent});
                    }

                    delete c.children;
                });

                _.each(categories, function(c) {
                    if (c.parent) {
                        c.parent.children || (c.parent.children = []);
                        c.parent.children.push(c);
                    }
                });

                var tree = [];
                function addToTree(category, indent) {
                    category.indent = indent;
                    tree.push(category);
                    _.each(category.children, function(c) {
                        addToTree(c, indent + 1);
                    });
                }
                _.each(categories, function(c) {
                    if (!c.parent) addToTree(c, 0);
                });
                return tree;
            }
		};
    }
]);
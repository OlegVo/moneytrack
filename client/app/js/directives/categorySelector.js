'use strict';

App.directive('categorySelector', function() {
	return {
		restrict: 'A',

		link: function($scope, element, attrs) {
            var allCategories = [];

            $scope.$watch(attrs.categorySelector, function(categories) {
                allCategories = categories;
                $scope.filteredCategories = categories;
                $scope.selectedCategoryIndex = null;
            });

            $scope.$watch('categoryName', function(value) {
                value = _.trim(value);

                if (!value.length) {
                    $scope.filteredCategories = allCategories;
                    $scope.selectedCategoryIndex = null;
                    return;
                }

                $scope.filteredCategories = _.filter(allCategories, function(c) {
                    return _.str.include(c.name.toLowerCase(), value.toLowerCase());
                });

                $scope.selectedCategoryIndex = 0;
            });

            var categoryInput = $(element[0]);
            var categoryList;

            function selectCategory(categoryElement) {
                if (!categoryElement && $scope.selectedCategoryIndex !== null) {
                    categoryElement = categoryList.find('li.active');
                }

                var category;
                if (categoryElement && categoryElement.data('id')) {
                    category = {
                        id: categoryElement.data('id'),
                        name: categoryElement.data('name')
                    };
                }
                $scope.categoryName = category ? category.name : '';
                $scope.selectCategory(category);
            }

            function moveToNextField() {
                setTimeout(function() {
                    var $nextField = categoryInput.parentsUntil('fieldset').next().children('input:eq(0)');
                    if ($nextField.length) $nextField.focus();
                    else categoryInput.blur();
                });
            }

            function unfocus() {
                setTimeout(function() {
                    categoryInput.blur();
                });
            }

            $scope.categoryInputFocus = function() {
                $scope.categoryListShown = true;
                $scope.categoryName = $scope.edited.category && $scope.edited.category.name || $scope.edited.parent && $scope.edited.parent.name || '';

                setTimeout(function() {
                    categoryList = $('#categoryList');
                    $('#categoryList').addClass('in');
                });
            };

            $scope.categoryInputKeydown = function(e) {
                switch (e.which) {
                    case 13: // enter
                        e.preventDefault();
                        e.stopPropagation();

                        selectCategory();
                        moveToNextField();
                        break;
                    case 38: // up
                        e.preventDefault();
                        if ($scope.selectedCategoryIndex) {
                            $scope.selectedCategoryIndex--;
                        }
                        break;
                    case 40: // down
                        e.preventDefault();
                        if ($scope.filteredCategories.length) {
                            if ($scope.selectedCategoryIndex === null) $scope.selectedCategoryIndex = 0;
                            else if ($scope.selectedCategoryIndex < $scope.filteredCategories.length - 1) $scope.selectedCategoryIndex++;
                        }
                        break;
                    case 9: // tab
                        selectCategory();
                        break;
                    case 27: // esc
                        unfocus();
                        break;
                }
            };

            $scope.categoryInputBlur = function() {
                categoryList.removeClass('in');

                setTimeout(function() {
                    if (!categoryList.hasClass('in')) {
                        $scope.$apply(function () {
                            $scope.categoryListShown = false;
                        });
                    }
                }, 200);
			};

            $scope.categoryListItemClick = function(e) {
                selectCategory($(e.currentTarget));
                moveToNextField();
            };

            $scope.clearCategory = function(e) {
                $scope.selectCategory(null);
                setTimeout(function() {
                    categoryInput.focus();
                });
            };

            $scope.newCategoryClick = function() {
                $scope.createCategory($scope.categoryName, function(category) {
                    $scope.selectCategory(category);
                    moveToNextField();
                });
            };
		}
	}
});

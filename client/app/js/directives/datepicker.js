
// Docs: http://bootstrap-datepicker.readthedocs.org/en/release/
App.directive('datepicker', function() {
	return {
		restrict: 'A',

		link: function ($scope, element) {
			var options = {
				format: 'dd.mm.yyyy',
				weekStart: 1,
				language: 'ru',
				autoclose: true,
				todayHighlight: true
			};

			setTimeout(function() {
				$(element).datepicker(options);
			});
		}
	};
});
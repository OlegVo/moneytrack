var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var runSequence = require('run-sequence');
var clean = require('gulp-clean');
var replace = require('gulp-replace');

var baseBuildDir = 'app/build';
var hash = Math.random().toString(36).substring(8);
var buildDir = baseBuildDir + '/' + hash;

gulp.task('clean', function () {
    return gulp.src(baseBuildDir, {read: false})
        .pipe(clean());
});

gulp.task('css', function () {
	gulp.src([
		'app/lib/datepicker/datepicker.css',
		'app/css/**/*.css'
	])
	.pipe(concat('app.css'))
	.pipe(gulp.dest(buildDir));
});

gulp.task('js', function () {
	gulp.src([
		'app/lib/angular/angular.min.js',
		'app/lib/**/*.min.js',
		'app/lib/moment/lang/ru.js',
		'app/lib/datepicker/bootstrap-datepicker.js',
		'app/lib/datepicker/bootstrap-datepicker.ru.js',
		'app/config/base.js',
		'app/js/app.js',
		'app/js/core/*.js',
		'app/js/*/*.js'
	])
	.pipe(concat('app.js'))
	.pipe(uglify())
	.pipe(gulp.dest(buildDir));
});

gulp.task('template', function(){
    gulp.src(['app/index_prod.html'])
        .pipe(replace(/("build\/)\w+(\/app\.[a-z]+")/g, '$1' + hash + '$2'))
        .pipe(gulp.dest('app'));
});

gulp.task('watch', ['js', 'css'], function() {
	gulp.watch('app/js/**/*.js', 'app/css/**/*.js', ['js', 'css']);
});

gulp.task('default', function(callback) {
	runSequence('clean', 'js', 'css', 'template', callback);
});
'use strict';

describe('controllers', function() {
    describe('ExpensesCtrl', function() {
        var scope, controller,
            date = 'today';

        beforeEach(function() {
            module('moneyTracker');

            inject(function($controller) {
                scope = {
                    date: date,
                    expenses: [{category: 1}, {category: 2}, {category: 3}]
                };
                controller = $controller('ExpensesCtrl', {$scope: scope});
            });
        });

        describe('addExpense', function() {
            it('добавляет новый элемент в expenses', function() {
                expect(scope.expenses.length).toBe(3);
                scope.addExpense();

                expect(scope.expenses.length).toBe(4);
            });

            it('новый элемент становится активным и редактируемым', function() {
                scope.addExpense();

                expect(_.last(scope.expenses).active).toBe(true);

                expect(scope.edited).toEqual(_.last(scope.expenses));
            });

            it('присваивает новому элементу дату', function() {
                scope.addExpense();

                expect(_.last(scope.expenses).date).toEqual(date);
            });
        });

        describe('editExpense', function() {
            it('делает выбранный элемент активным и редактируемым', function() {
                scope.editExpense.call({$index: 1});

                expect(scope.expenses[1].active).toBe(true);
                expect(scope.edited).toEqual(scope.expenses[1]);
            });

            it('делает неактивными остальные элементы', function() {

            });

            it('удаляет элементы с пустой категорией и суммой', function() {
                scope.expenses = [{category: 'adfadf'}, {category: ''}];

                scope.editExpense(0);

                expect(scope.expenses.length).toBe(1);
            });

            it('пустой элемент не должен удаляться, если его сейчас редактируем', function() {
                scope.expenses = [];

                scope.addExpense();

                expect(scope.expenses.length).toBe(1);

                scope.editExpense.call({$index: 0});

                expect(scope.expenses.length).toBe(1);
            });
        });

        describe('deleteExpense', function() {
            it('удаляет редактируемый элемент', function() {
                scope.edited = scope.expenses[1];
                scope.deleteExpense();

                expect(scope.expenses.length).toBe(2);

                expect(scope.expenses[0].category).toBe(1);
                expect(scope.expenses[1].category).toBe(3);
            });

            it('устанавливает редактируемым следующий элемент', function() {
                scope.edited = scope.expenses[1];
                scope.deleteExpense();

                expect(scope.edited).toEqual(scope.expenses[1]);
            });

            it('устанавливает редактируемым предыдущий элемент, если удаляется последний', function() {
                scope.edited = _.last(scope.expenses);
                scope.deleteExpense();

                expect(scope.edited).toEqual(_.last(scope.expenses));
            });

            it('сбрасывает редактируемый элемент, если удалены все', function() {
                scope.expenses = [scope.expenses[0]];
                scope.edited = scope.expenses[0];
                scope.deleteExpense();

                expect(scope.expenses.length).toBe(0);
                expect(scope.edited).toBeFalsy();
            });
        });
    });
});
